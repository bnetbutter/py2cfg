#!/bin/bash


cd examples
rm *.png

for file in *.py
do
    echo "Running: python3 ../py2cfg/_runner.py $file"
    python3 ../py2cfg/_runner.py $file
done
